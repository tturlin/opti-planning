import sqlite3
import csv

conn = sqlite3.connect('database.db')
c = conn.cursor()

data = []
with open("BD/Infirmiere.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS infirmiere ({} integer PRIMARY KEY, {} text NOT NULL)".format(data[0][0], data[0][1]))
for i in range(1, len(data)):
    c.execute("INSERT INTO infirmiere VALUES (?, ?)",(int(data[i][0]), data[i][1]))

conn.commit()

data = []
with open("BD/Infirmiere-Lit.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS infirmiere_lit ({} integer, {} integer, {} text, {} real, {} real, PRIMARY KEY({}, {}))".format(data[0][0],  data[0][1], data[0][2], data[0][3], data[0][4], data[0][0],  data[0][1]))
for i in range(1, len(data)):
    c.execute("INSERT INTO infirmiere_lit VALUES (?, ?, ?, ?, ?)", (int(data[i][0]), int(data[i][1]), data[i][2], float(data[i][3]), float(data[i][4])))

conn.commit()

data = []
with open("BD/Lit.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS lit ({} integer PRIMARY KEY)".format(data[0]))
for i in range(1, len(data)):
    c.execute("INSERT INTO lit VALUES (?)", (data[i][0]))

conn.commit()

data = []
with open("BD/Patient.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS patient ({} integer PRIMARY KEY, {} text, {} real, {} real)".format(data[0][0], data[0][1], data[0][2], data[0][3]))
for i in range(1, len(data)):
    c.execute("INSERT INTO patient VALUES (?, ?, ?, ?)", (int(data[i][0]), data[i][1], float(data[i][2]), float(data[i][3])))

conn.commit()

data = []
with open("BD/Planning.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS planning ({} integer, {} integer, {} text, {} integer, {} real, {} real)".format(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4], data[0][5]))

conn.commit()

data = []
with open("BD/Protocole-Traitement.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS protocole_traitement ({} integer, {} integer, PRIMARY KEY({}, {}))".format(data[0][0], data[0][1], data[0][0], data[0][1]))
for i in range(1, len(data)):
    c.execute("INSERT INTO protocole_traitement VALUES (?, ?)", (int(data[i][0]), int(data[i][1])))

conn.commit()

data = []
with open("BD/Protocole.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS protocole ({} integer PRIMARY KEY, {} text, {} real, {} real, {} real)".format(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4]))
for i in range(1, len(data)):
    c.execute("INSERT INTO protocole VALUES (?, ?, ?, ?, ?)",(int(data[i][0]), data[i][1], float(data[i][2]), float(data[i][3]), float(data[i][4])))

conn.commit()

data = []
with open("BD/Soin.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS soins ({} integer PRIMARY KEY AUTOINCREMENT, {} integer, {} text, {} real, {} integer, {} integer, {} integer, {} real, {} real)".format(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4], data[0][5], data[0][6], data[0][7], data[0][8]))

conn.commit()

data = []
with open("BD/Traitement.txt") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    for row  in spamreader:
        data.append(row)

c.execute("CREATE TABLE IF NOT EXISTS traitement ({} integer PRIMARY KEY, {} text, {}real, {} integer)".format(data[0][0], data[0][1], data[0][2], data[0][3]))
for i in range(1, len(data)):
    c.execute("INSERT INTO traitement VALUES (?, ?, ?, ?)", (int(data[i][0]), data[i][1], float(data[i][0]), int(data[i][3])))

conn.commit()

conn.close()