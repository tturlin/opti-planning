from datetime import date as d

work_time = 10+1/3
places = 33
charge = 0.8
protocol_min = 5/60

dispo_init = work_time * places * charge
dispo = dispo_init

def is_possible(time, dispo, date):
    delta = date - d.today()
    if delta.days >= 7:
        if (dispo - time) > 0:
            return True
        else:
            print("Il n'y a plus assez de volume horaire pour placer ce rendez-vous le {}".format(date))
            return False
    else:
        print("Le planning de cette journée est déja terminé.")
        return False


def state(dico, date, dispo):
    time = sum(dico[date][1])
    print("Il y a {} rendez-vous le {} et il reste {}h:{}min de volume traitement.".format(len(dico[date][0]), date, int(dispo), int((dispo-int(dispo))*60)))

def dispo_(dico, date, dispo_init):
    try:
        return dispo_init - sum(dico[date][1])
    except KeyError:
        return dispo_init


def rdv(dico, dispo_init, protocol_min):
    date = input('Entrez la date de l\'emploi du temps voulu (jj/mm/aaaa) :\n')
    dispo = dispo_(dico, date, dispo_init)

    try:
        state(dico, date, dispo)
    except KeyError:
        print("Il n'y a pas de rendez-vous le {}, il reste donc {}h:{}min de volume traitement.".format(date, int(dispo), int((dispo-int(dispo))*60)))
    while dispo < protocol_min:
        date = input('Entrez la date de l\'emploi du temps voulu (jj/mm/aaaa) :\n')

    name = input('Entrez le nom du patient :\n')
    time = float(input('Entrez le temps de prise en charge (en heure) :\n'))
    protocol = input("Quel est le protocole :\n")
    ponderation = float(input("Quelle est l'importance du rendez-vous ?:\n"))
    visite = int(input("Visite nécéssaire ? :\n"))
    h_mini = input("Heure minimum pour le rendez-vous (hh:mm) :\n")
    h_maxi = input("Heure maximum pour le rendez-vous (hh:mm) :\n")

    h_mini = h_mini.split(":")
    h_mini = int(h_mini[0]) + int(h_mini[1]) / 60

    h_maxi = h_maxi.split(":")
    h_maxi = int(h_maxi[0]) + int(h_maxi[1]) / 60

    try:
        dico[date] += 1
    except TypeError:
        pass
    except KeyError:
        dico[date] = [[], [], [], [], [], [], []]  # [[Name], [Duration], [Protocol], [Ponderation], [visite], [Hmin], [Hmax]]

    date_info = date.split('/')
    date_f = d(day=int(date_info[0]), month=int(date_info[1]), year=int(date_info[2]))

    if is_possible(time, dispo, date_f):
        dico[date][0].append(name)
        dico[date][1].append(time)
        dico[date][2].append(protocol)
        dico[date][3].append(ponderation)
        dico[date][4].append(visite)
        dico[date][5].append(h_mini)
        dico[date][6].append(h_maxi)
        dispo = dispo_(dico, date, dispo_init)
        print("Le traitement de {} est bien enregistré au planning prévisonnel pour une durée de {}h:{}min.".format(name, int(time), int((time-int(time))*60)))
        state(dico, date, dispo)

    return dico, dispo


def edition(dico, dispo_init,protocol_min):
    date = input('Quelle date voulez-vous modifier ? (jj/mm/aaaa)\n')
    name = input('Entrez le nom du patient à modifier :\n')
    name_ = dico[date][0]
    it_name = name_.index(name)
    for i in range(len(dico[date])):
        dico[date][i].remove(dico[date][i][it_name])

    dispo = dispo_(dico, date, dispo_init)
    print('Nous allons maintenant procéder à la nouvelle date de rendez-vous.')
    dico, dispo = rdv(dico, dispo_init, protocol_min)
    return dico, dispo


def validInputs(question, *validInputs):
        verification = input(question)
        while verification not in validInputs:
                verification = input(question)
        return verification


patient = dict()
while True:
    choice = int(validInputs("Voulez-vous créer un rendez-vous (1) ou modifier un rendez-vous (2) ou quitter le programme (3) ?\n", '1', '2', '3'))
    if choice == 1:
        patient, dispo = rdv(patient, dispo, protocol_min)
    elif choice == 2:
        patient, dispo = edition(patient, dispo_init,protocol_min)
    else:
        with open('patient.csv', newline='', mode='w') as csvfile:
            csvfile.write('#Date, Nom, '+'Durée, '+'Protocole, '+'Importance, '+'Visite, '+'Heure mini, '+'Heure max'+'\n')
            for key in patient.keys():
                for j in range(len(patient[key][0])):
                    csvfile.write(key)
                    csvfile.write(',')
                    for i in range(len(patient[key])):
                        csvfile.write(str(patient[key][i][j]))
                        if i != (len(patient[key])-1):
                            csvfile.write(',')
                    csvfile.write('\n')
        break
