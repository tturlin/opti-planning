from datetime import date as d
import sqlite3

work_time = 10+1/3
places = 33
charge = 0.8
protocol_min = 5/60

dispo_init = work_time * places * charge
dispo = dispo_init

conn = sqlite3.connect('database.db')
c = conn.cursor()


def is_possible(c,time, dispo, date):
    delta = date - d.today()
    if delta.days >= 7:
        if (dispo - time) > 0:
            return True
        else:
            print("Il n'y a plus assez de volume horaire pour placer ce rendez-vous le {}".format(date))
            return False
    else:
        print("Le planning de cette journée est déja terminé.")
        return False


def state(c,date,dispo):
    t=(date,)
    time=[]
    for row in c.execute('SELECT Durée_soin FROM soins WHERE Date_soin=?', t):
        time.append(row)
    time_ = sum(time[:][0])

    print("Il y a {} rendez-vous le {} et il reste {}h:{}min de volume traitement.".format(len(time), date, int(dispo), int((dispo-int(dispo))*60)))
    return time_

def dispo_(c, date, dispo_init):
    t=(date,)
    time=[]
    for row in c.execute('SELECT Durée_soin FROM soins WHERE Date_soin=?', t):
        time.append(row)
    try:
        time = sum(time[:][0])
    except:
        time = 0
    
    available = dispo_init-time if time != 0 else dispo_init
    return available


def rdv(c):
    date = input('Entrez la date de l\'emploi du temps voulu (jj/mm/aaaa) :\n')
    dispo = dispo_(c,date, dispo_init)

    name = input('Entrez le nom du patient :\n')
    name_id = int(input('Entrez le numéro du patient :\n'))
    time = float(input('Entrez le temps de prise en charge (en heure) :\n'))
    protocol = input("Quel est le protocole :\n")
    ponderation = float(input("Quelle est l'importance du rendez-vous ?:\n"))
    visite = int(validInputs("Visite nécessaire : Oui = 1, Non = 0 ?\n", '0', '1'))
    h_mini = input("Heure minimum pour le rendez-vous (hh:mm) :\n")
    h_maxi = input("Heure maximum pour le rendez-vous (hh:mm) :\n")

    h_mini = h_mini.split(":")
    h_mini = int(h_mini[0]) + int(h_mini[1]) / 60

    h_maxi = h_maxi.split(":")
    h_maxi = int(h_maxi[0]) + int(h_maxi[1]) / 60

    
    date_info = date.split('/')
    date_f = d(day=int(date_info[0]), month=int(date_info[1]), year=int(date_info[2]))

    if is_possible(c,time, dispo, date_f):
        c.execute("INSERT INTO soins VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(None, name_id, date, time, protocol, ponderation, visite, h_mini, h_maxi))
        conn.commit()
        dispo = dispo_(c, date, dispo_init)
        print("Le traitement de {} est bien enregistré au planning prévisonnel pour une durée de {}h:{}min.".format(name, int(time), int((time-int(time))*60)))
        state(c, date, dispo)
        c.execute("INSERT INTO patient VALUES (?, ?, ?, ?)", (name_id, name, h_mini, h_maxi))
    
    return dispo
    

def edition(c, dispo_init):
    date = input('Quelle date voulez-vous modifier ? (jj/mm/aaaa)\n')
    no = input('Entrez le numéro du patient à modifier :\n')
    c.execute("DELETE from soins where  No_patient=? & Date_soin=?",(no,date))

    dispo = dispo_(c, date, dispo_init)
    print('Nous allons maintenant procéder à la nouvelle date de rendez-vous.')
    dispo = rdv(c)
    return dispo


def validInputs(question, *validInputs):
        verification = input(question)
        while verification not in validInputs:
                verification = input(question)
        return verification

edition(c,dispo_init)
# patient = dict()
# while True:
#     choice = int(validInputs("Voulez-vous créer un rendez-vous (1) ou modifier un rendez-vous (2) ou quitter le programme (3) ?\n", '1', '2', '3'))
#     if choice == 1:
#         patient, dispo = rdv(patient, dispo, protocol_min)
#     elif choice == 2:
#         patient, dispo = edition(patient, dispo_init,protocol_min)
#     else:
#         with open('patient.csv', newline='', mode='w') as csvfile:
#             csvfile.write('#Date, '+'Nom, '+'Durée, '+'Protocole, '+'Importance, '+'Visite, '+'Heure mini, '+'Heure max'+'\n')
#             for key in patient.keys():
#                 for j in range(len(patient[key][0])):
#                     csvfile.write(key)
#                     csvfile.write(',')
#                     for i in range(len(patient[key])):
#                         csvfile.write(str(patient[key][i][j]))
#                         if i != (len(patient[key])-1):
#                             csvfile.write(',')
#                     csvfile.write('\n')
#         break
